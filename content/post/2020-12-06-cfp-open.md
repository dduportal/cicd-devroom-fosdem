---
title: CICD Devroom CFP
subtitle: FOSDEM 2021
date: 2020-12-06
tags: ["cfp","2021"]
---

# CICD Devroom Call For Proposal - FOSDEM 2021

I am happy to share that the CFP for the CI/CD devroom at FOSDEM 2021, to be held on the 7th of February 2021, is now open. Please note that this year FOSDEM will be online.

We are looking for contributions in the form of presentations (50min) or lightning talks (20min).

Possible topics include:



*   Project showcases, modern tooling
*   Stories from end-users (e.g. success/failure)  about:
    *   Continuous Integration
    *   Continuous Delivery
    *   Continuous Deployment
*   Interesting use-cases
*   Pipeline standardization
*   BoF sessions

Remark: This may sound obvious but we will only accept talks covering open-source.

Considering the event is online this year, the format of each presentation will be adapted. While the recorded presentation is streaming, attendees will be able to ask questions in a chat. The speaker will have the opportunity to answer them live or prepare better answers for the live Q&A that follow the presentation.  Once the session is over, a breakout room will be available to continue discussions with whoever is interested.

**Submitting talks:**

Please submit your proposals by the 27th of December 2020.

**Presentation details:**

*   The default duration for talks will be 50 minutes including Q&A. Feel free to request a shorter slot of 20 minutes if it's more appropriated.
*   Accepted presentations must be recorded and uploaded on Pentabarf by the 17 of January 2021.
*   Submissions are accepted via the [FOSDEM 2021](https://penta.fosdem.org/submission/FOSDEM21) website. Please do not recreate your pentabarf account if you already have one.

Make sure to include the following information in your proposal:

*   First Name and Last Name
*   Photo
*   Biography
*   Email Address
*   A mobile phone number that we can use to contact you in case of an emergency
*   Title and subtitle of your talk
*   A short abstract (one or two paragraphs) detailing what the talk will discuss, and why attendees should attend it
*   A longer description if you have more details to share
*   Select the "Continuous Integration and Continuous Deployment devroom" track!
*   The duration of your talk (20 or 50 min, including time for Q&A)
*   links to related material/websites/blogs (optional)

**Important dates:**

*   27th of December 2020: Deadline for submission of proposals
*   31th of December 2020: Announcement of the final schedule
*   17th of January, 2021: Presentation uploaded on Pentabarf
*   24th of January, 2021: Presentation validated by moderators
*   7th of February, 2021: devroom day from 8 AM UTC to 4 PM UTC


Talks will be recorded and published under the same license as all FOSDEM content (CC-BY).

By agreeing to present at FOSDEM, you automatically give permission to be recorded.

**Accepted presentation:**

Accepted speakers will be assigned a moderator who will help to prepare everything.

**We need volunteers:**

We are looking for volunteers to help us review talks, approve uploaded videos, and moderate each session. Feel free to contact us for more information.

If you have any issues or questions, contact [cicd-devroom-manager@fosdem.org](mailto:cicd-devroom-manager@fosdem.org)

**Links:**

* [FOSDEM website](https://fosdem.org/2021/)  
* [FOSDEM code of conduct](https://fosdem.org/2021/practical/conduct/)  
* [cicd-devroom-manager@fosdem.org](mailto:cicd-devroom-manager@fosdem.org)
* [CFP Submission](https://penta.fosdem.org/submission/FOSDEM21) 


